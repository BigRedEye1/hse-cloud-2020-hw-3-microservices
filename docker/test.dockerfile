FROM rust:1.49-buster AS build
WORKDIR /src
COPY Cargo.lock Cargo.toml ./
# FIXME
COPY . .
RUN cargo test --no-run
CMD ["cargo", "test"]

