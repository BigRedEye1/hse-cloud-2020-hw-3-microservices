FROM rust:1.49-buster AS build
WORKDIR /src
COPY Cargo.lock Cargo.toml ./
# FIXME
COPY . .
RUN cargo build --release --locked


FROM debian:buster-slim
ARG CRATE
RUN apt-get update
RUN apt-get install -y ca-certificates
RUN update-ca-certificates
COPY --from=build /src/target/release/* ./

ENV RUST_LOG info
CMD ["./app"]

