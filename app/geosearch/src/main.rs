use prelude::*;

use actix_web::{post, web, App, HttpServer, HttpResponse, Responder};
use std::str::FromStr;

#[derive(Clone, Debug, serde::Deserialize)]
struct Config {
    database: String,
    bind_address: String,
}

#[derive(Clone)]
struct GeoSearch {
    conf: Config,
    database: std::sync::Arc<Vec<(ipnet::Ipv4Net, String)>>,
}

fn load_database<DB: mock::DataBaseProvider>(conf: &Config, provider: DB) -> Result<Vec<(ipnet::Ipv4Net, String)>> {
    let mut db: Vec<(ipnet::Ipv4Net, String)> = Vec::new();

    let file = provider.load(&conf.database)?;
    let mut rdr = csv::Reader::from_reader(file.as_bytes());
    for result in rdr.deserialize() {
        let record: api::GeoRecord = result?;

        db.push((ipnet::Ipv4Net::from_str(&record.network)?, record.country_name));
    }

    Ok(db)
}

#[async_trait::async_trait(?Send)]
impl service::Service for GeoSearch {
    type Config = Config;

    fn name() -> &'static str {
        "geosearch"
    }

    async fn new<DB: mock::DataBaseProvider>(conf: Self::Config, db: DB) -> Result<Self> {
        let db = load_database(&conf, db)?;
        Ok(GeoSearch{
            conf,
            database: std::sync::Arc::new(db),
        })
    }

    async fn run(self) -> Result<()> {
        log::info!("Starting geosearch server at {}", &self.conf.bind_address);

        let clone = self.clone();
        HttpServer::new(move || App::new().data(clone.clone()).service(search))
            .bind(self.conf.bind_address)?
            .run()
            .await?;

        Ok(())
    }
}

impl GeoSearch {
    async fn search(&self, req: api::GeoRequest) -> Result<api::GeoResponse> {
        let ip = std::net::Ipv4Addr::from_str(&req.ip_addr)?;
        let res: Option<&String> = self.database.iter().filter(|(network, _res)| network.contains(&ip)).take(1).map(|(_, res)| res).next();
        Ok(api::GeoResponse{region: res.map(|e| e.clone())})
    }
}

#[post("/search")]
async fn search(geosearch: web::Data<GeoSearch>, web::Json(request): web::Json<api::GeoRequest>) -> impl Responder {
    match geosearch.search(request).await {
        Ok(result) => HttpResponse::Ok().json(result),
        Err(error) => {
            log::error!("Search failed: {}", error);
            HttpResponse::InternalServerError().json(api::Error{ error: error.to_string() })
        }
    }
}

#[actix_web::main]
async fn main() {
    service::run::<GeoSearch, mock::FileSystemDataBase>(mock::FileSystemDataBase::new()).await;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[actix_rt::test]
    async fn test_service() {
        let thread = std::thread::spawn(|| {
            mock::wait_for("http://localhost:12346").unwrap();
            let res = mock::request::<api::GeoRequest, api::GeoResponse>("http://localhost:12346/search", api::GeoRequest{
                ip_addr: "1.1.1.1".into(),
            }).unwrap();

            assert_eq!(res, api::GeoResponse{ region: Some("Australia".into()) });

            nix::sys::signal::raise(nix::sys::signal::Signal::SIGINT).unwrap();
        });

        let cfg = Config {
            bind_address: "localhost:12346".into(),
            database: "db.csv".into(),
        };
        let mut db = mock::DataBaseMock::new();
        db.add("db.csv".into(), r#"network,geoname_id,continent_code,continent_name,country_iso_code,country_name,is_anonymous_proxy,is_satellite_provider
1.1.1.0/24,2077456,OC,Oceania,AU,Australia,False,False
104.143.19.0/24,49518,AF,Africa,RW,Rwanda,False,False
217.194.222.0/25,,,,,,True,False
"#.into());

        service::run_with_config::<GeoSearch, mock::DataBaseMock>(db, cfg).await;
        thread.join().unwrap();
    }
}
