use prelude::*;

use actix_web::{post, web, App, HttpServer, HttpResponse, Responder};

#[derive(Clone, Debug, serde::Deserialize)]
struct Config {
    replicas: String,
    bind_address: String,
}

#[derive(Clone)]
struct IntSearch {
    conf: Config,
    replicas: Vec<(reqwest::Client, String)>,
}

#[async_trait::async_trait(?Send)]
impl service::Service for IntSearch {
    type Config = Config;

    fn name() -> &'static str {
        "intsearch"
    }

    async fn new<DB: mock::DataBaseProvider>(conf: Self::Config, _db: DB) -> Result<Self> {
        let replicas = conf.replicas
            .split(',')
            .map(|url| {
                let client = reqwest::ClientBuilder::new()
                    .timeout(std::time::Duration::from_millis(1000))
                    .build().unwrap();
                (client, url.into())
            })
            .collect::<Vec<_>>();
        Ok(IntSearch{
            conf,
            replicas,
        })
    }

    async fn run(self) -> Result<()> {
        log::info!("Starting intsearch server at {}", &self.conf.bind_address);

        let clone = self.clone();
        HttpServer::new(move || App::new().data(clone.clone()).service(search))
            .bind(self.conf.bind_address)?
            .run()
            .await?;

        Ok(())
    }
}

impl IntSearch {
    async fn request(req: reqwest::RequestBuilder) -> Result<api::SearchResult> {
        let res = req
            .send()
            .await?
            .json()
            .await?;
        Ok(res)
    }

    async fn search(&self, request: api::BaseSearchRequest) -> Result<api::SearchResult> {
        let mut requests = Vec::new();
        for req in self.replicas.iter().map(|replica| replica.0.post(&replica.1).json(&request)) {
            requests.push(Self::request(req));
        }
        let results = futures::future::join_all(requests).await.into_iter().collect::<Result<Vec<api::SearchResult>, _>>()?;
        let mut heap = rank::DocHeap::new(request.limit.unwrap_or(10) as usize);

        for res in &results {
            for doc in &res.search_results {
                heap.add(rank::WeightedDoc{
                    document: doc,
                    relevance: rank::Relevance{
                        matched_tokens: doc.relevance.as_ref().unwrap().matched_terms,
                        rating: doc.relevance.as_ref().unwrap().rating,
                        key_md5: &doc.key_md5,
                    }
                });
            }
        }

        let ans = heap.finish().into_iter().map(|rank::WeightedDoc{document, relevance: _}| document.clone()).collect::<Vec<_>>();
        Ok(api::SearchResult{
            search_results: ans,
        })
    }
}

#[post("/search")]
async fn search(intsearch: web::Data<IntSearch>, web::Json(request): web::Json<api::BaseSearchRequest>) -> impl Responder {
    match intsearch.search(request).await {
        Ok(result) => HttpResponse::Ok().json(result),
        Err(error) => {
            log::error!("Search failed: {}", error);
            HttpResponse::InternalServerError().json(api::Error{ error: error.to_string() })
        }
    }
}

#[actix_web::main]
async fn main() {
    service::run::<IntSearch, mock::FileSystemDataBase>(mock::FileSystemDataBase::new()).await;
}
