use prelude::*;

use actix_web::{get, web, App, HttpServer, HttpResponse, Responder};

#[derive(Clone, Debug, serde::Deserialize)]
struct Config {
    geosearch: String,
    usersearch: String,
    intsearch: String,
    bind_address: String,
}

#[derive(Clone)]
struct MetaSearch {
    conf: Config,
    client: reqwest::Client,
}

#[async_trait::async_trait(?Send)]
impl service::Service for MetaSearch {
    type Config = Config;

    fn name() -> &'static str {
        "metasearch"
    }

    async fn new<DB: mock::DataBaseProvider>(conf: Self::Config, _db: DB) -> Result<Self> {
        Ok(MetaSearch{
            conf,
            client: reqwest::ClientBuilder::new()
                .timeout(std::time::Duration::from_millis(1000))
                .build().unwrap(),
        })
    }

    async fn run(self) -> Result<()> {
        log::info!("Starting metasearch server at {}", &self.conf.bind_address);

        let clone = self.clone();
        HttpServer::new(move || App::new().data(clone.clone()).service(search))
            .bind(self.conf.bind_address)?
            .run()
            .await?;

        Ok(())
    }
}

impl MetaSearch {
    async fn request<Req, Res>(&self, url: &str, req: Req) -> Result<Res>
        where Req: serde::ser::Serialize + Sized,
              Res: serde::de::DeserializeOwned,
    {
        let res: Res = self.client.post(url).json(&req).send().await?.json().await?;
        Ok(res)
    }

    async fn search(&self, request: api::MetaSearchRequest) -> Result<api::MetaSearchResponse> {
        let user_request = self.request::<api::UserRequest, api::UserResponse>(&self.conf.usersearch, api::UserRequest{user_id: request.user_id});
        let geo_request = self.request::<api::GeoRequest, api::GeoResponse>(&self.conf.geosearch, api::GeoRequest{ip_addr: request.ip_addr});
        let (user, geo) = futures::future::join(user_request, geo_request).await;
        let user = user?;
        let geo = geo?;

        let mut res = self.request::<api::BaseSearchRequest, api::SearchResult>(&self.conf.intsearch, api::BaseSearchRequest{
            text: request.text,
            limit: Some(request.limit.unwrap_or(10)),
            geo: Some(geo),
            user: Some(user),
        }).await?;

        for doc in &mut res.search_results {
            doc.relevance = None;
        }

        Ok(api::MetaSearchResponse::SearchResults(res.search_results))
    }
}

#[get("/search")]
async fn search(metasearch: web::Data<MetaSearch>, web::Query(request): web::Query<api::MetaSearchRequest>) -> impl Responder {
    match metasearch.search(request).await {
        Ok(result) => HttpResponse::Ok().json(result),
        Err(error) => {
            log::error!("Search failed: {}", error);
            HttpResponse::InternalServerError().json(api::MetaSearchResponse::Error(api::Error{
                error: "Internal server error".into(),
            }))
        }
    }
}

#[actix_web::main]
async fn main() {
    service::run::<MetaSearch, mock::FileSystemDataBase>(mock::FileSystemDataBase::new()).await;
}
