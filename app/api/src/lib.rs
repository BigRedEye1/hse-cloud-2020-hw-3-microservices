use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize)]
pub struct Error {
    pub error: String,
}

#[derive(Serialize, Deserialize)]
pub struct NewsRecord {
    pub class: String,
    pub key: String,
    pub document: String,
    pub age_from: u32,
    pub age_to: u32,
    pub region: String,

    #[serde(rename="gender")]
    pub genders: String,
    pub key_md5: String,
}

#[derive(Serialize, Deserialize)]
pub struct UserRecord {
    pub user_id: u32,
    pub gender: String,
    pub age: u32,
}

#[derive(Serialize, Deserialize)]
pub struct GeoRecord {
    pub network: String,
    pub geoname_id: String,
    pub continent_code: String,
    pub continent_name: String,
    pub country_iso_code: String,
    pub country_name: String,
    pub is_anonymous_proxy: String,
    pub is_satellite_provider: String,
}

#[derive(Serialize, Deserialize)]
pub struct MetaSearchRequest {
    pub text: String,
    pub user_id: u32,
    pub ip_addr: String,
    pub limit: Option<u32>,
}

#[derive(Serialize, Deserialize)]
pub enum MetaSearchResponse {
    Error(Error),
    SearchResults(Vec<Document>),
}

#[derive(Serialize, Deserialize)]
pub struct SearchResult {
    pub search_results: Vec<Document>,
}

#[derive(Serialize, Deserialize, Clone, Debug, Eq, PartialEq)]
pub struct Document {
    pub document: String,
    pub key: String,
    pub key_md5: String,

    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub relevance: Option<Relevance>,
}

#[derive(Serialize, Deserialize, Clone, Debug, Eq, PartialEq)]
pub struct Relevance {
    pub matched_terms: u32,
    pub rating: u32,
}

#[derive(Serialize, Deserialize)]
pub struct GeoRequest {
    pub ip_addr: String,
}

#[derive(Serialize, Deserialize, Debug, Eq, PartialEq)]
pub struct GeoResponse {
    pub region: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct UserRequest {
    pub user_id: u32,
}

#[derive(Serialize, Deserialize, Debug, Eq, PartialEq)]
pub struct UserResponse {
    pub gender: Option<String>,
    pub age: Option<u32>,
}

#[derive(Serialize, Deserialize)]
pub struct BaseSearchRequest {
    pub text: String,
    pub geo: Option<GeoResponse>,
    pub user: Option<UserResponse>,
    pub limit: Option<u32>,
}
