use std::collections::{HashMap, BinaryHeap};

#[derive(Copy, Clone, Ord, Eq, PartialEq, PartialOrd)]
pub struct DocId(pub u32);

pub struct InvertedIndex {
    terms: HashMap<String, Vec<DocId>>,
}

impl InvertedIndex {
    pub fn new() -> InvertedIndex {
        InvertedIndex{
            terms: HashMap::new(),
        }
    }

    pub fn add_document(&mut self, text: &str, docid: DocId) {
        for token in text.split(' ') {
            let gut = self.terms.entry(token.into()).or_insert(Vec::new());
            if gut.last() != Some(&docid) {
                gut.push(docid);
            }
        }
    }

    pub fn make_iterator(&self, query: &str, quorum: usize) -> impl std::iter::Iterator<Item=(DocId, u32)> + '_ {
        let mut iterators = Vec::new();
        for token in query.split(' ') {
            match self.terms.get_key_value(token) {
                None => continue,
                Some((term, gut)) => iterators.push(TermIterator::new(gut, term)),
            }
        }

        QuorumIterator::new(iterators, quorum)
    }
}

struct TermIterator<'a> {
    iter: std::slice::Iter<'a, DocId>,
    next: Option<DocId>,
    term: &'a str,
}

impl<'a> TermIterator<'a> {
    fn new(gut: &'a Vec<DocId>, term: &'a str) -> Self {
        let mut result = TermIterator{ iter: gut.iter(), next: None, term };
        result.next();
        result
    }

    fn peek(&self) -> Option<DocId> {
        self.next
    }
}

impl std::iter::Iterator for TermIterator<'_> {
    type Item = DocId;

    fn next(&mut self) -> Option<Self::Item> {
        let tmp = self.next;
        self.next = self.iter.next().map(|doc| *doc);
        tmp
    }
}

impl Ord for TermIterator<'_> {
    fn cmp<'a, 'b>(&'a self, other: &'b Self) -> std::cmp::Ordering {
        other.next.cmp(&self.next)
    }
}

impl PartialEq for TermIterator<'_> {
    fn eq<'a, 'b>(&'a self, other: &'b Self) -> bool {
        self.next.eq(&other.next)
    }
}

impl<'a, 'b> Eq for TermIterator<'a> {
}

impl PartialOrd for TermIterator<'_> {
    fn partial_cmp<'a, 'b>(&'a self, other: &'b Self) -> Option<std::cmp::Ordering> {
        other.next.partial_cmp(&self.next)
    }
}

pub struct QuorumIterator<'a> {
    heap: BinaryHeap<TermIterator<'a>>,
    size: usize,
    quorum: usize,
}

impl<'a> QuorumIterator<'a> {
    fn empty() -> Self {
        QuorumIterator{ heap: BinaryHeap::new(), size: 0, quorum: 0 }
    }

    fn new(iterators: Vec<TermIterator<'a>>, quorum: usize) -> Self {
        let mut res = Self::empty();
        res.size = iterators.len();
        res.quorum = quorum;
        for iterator in iterators.into_iter() {
            res.heap.push(iterator);
        }
        res
    }
}

impl std::iter::Iterator for QuorumIterator<'_> {
    type Item = (DocId, u32);

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            let mut iter = match self.heap.pop() {
                None => return None,
                Some(iter) => iter,
            };

            let doc = match iter.next() {
                None => continue,
                Some(doc) => doc,
            };
            let mut matched_tokens = Vec::new();
            matched_tokens.push(iter.term);

            self.heap.push(iter);

            let mut quorum = 1;
            while quorum < self.size {
                let iter = match self.heap.peek() {
                    None => break,
                    Some(iter) => iter,
                };
                let next_doc = match iter.peek() {
                    Some(doc) => doc,
                    None => break,
                };

                if next_doc > doc {
                    break;
                }
                assert!(next_doc == doc);

                quorum += 1;
                matched_tokens.push(iter.term);

                // Unwrap is safe self.heap.peek() above returned Some(iter)
                let mut iter = self.heap.pop().unwrap();
                iter.next();
                self.heap.push(iter);
            }

            if quorum >= self.quorum {
                return Some((doc, quorum as u32));
            }
        }
    }
}
