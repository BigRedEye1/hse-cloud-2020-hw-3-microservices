use prelude::*;

mod config;
mod database;
mod inverted_index;

use actix_web::{post, web, App, HttpServer, HttpResponse, Responder};

#[derive(Clone)]
struct BaseSearch {
    db: std::sync::Arc<database::DataBase>,
    conf: config::Config,
}

#[async_trait::async_trait(?Send)]
impl service::Service for BaseSearch {
    type Config = config::Config;

    fn name() -> &'static str {
        "basesearch"
    }

    async fn new<DB: mock::DataBaseProvider>(config: Self::Config, db: DB) -> Result<Self> {
        Ok(BaseSearch{
            db: std::sync::Arc::new(database::DataBase::new(&config, db)?),
            conf: config,
        })
    }

    async fn run(self) -> Result<()> {
        log::info!("Starting basesearch server at {}", &self.conf.bind_address);

        let clone = self.clone();
        HttpServer::new(move || App::new().data(clone.clone()).service(search))
            .bind(self.conf.bind_address)?
            .run()
            .await?;

        Ok(())
    }
}

const DEFULT_LIMIT: u32 = 10;

impl BaseSearch {
    fn search(&self, request: api::BaseSearchRequest) -> Result<api::SearchResult> {
        let restrictions = database::Restrictions{
            age: request.user.as_ref().and_then(|user| user.age),
            gender: request.user.and_then(|user| user.gender),
            region: request.geo.and_then(|geo| geo.region),
        };
        let limit = request.limit.unwrap_or(DEFULT_LIMIT) as usize;

        let mut results = api::SearchResult{ search_results: Vec::new() };
        for rank::WeightedDoc{document, relevance} in self.db.search(&request.text, limit, Some(&restrictions)).into_iter() {
            let res = api::Document{
                document: document.body.clone(),
                key: document.key.clone(),
                key_md5: document.key_md5.clone(),
                relevance: Some(api::Relevance{
                    matched_terms: relevance.matched_tokens,
                    rating: relevance.rating,
                }),
            };
            results.search_results.push(res);
        }

        Ok(results)
    }
}

#[post("/search")]
async fn search(basesearch: web::Data<BaseSearch>, web::Json(request): web::Json<api::BaseSearchRequest>) -> impl Responder {
    match basesearch.search(request) {
        Ok(result) => HttpResponse::Ok().json(result),
        Err(error) => {
            log::error!("Search failed: {}", error);
            HttpResponse::InternalServerError().json(api::Error{ error: error.to_string() })
        }
    }
}

#[actix_web::main]
async fn main() {
    service::run::<BaseSearch, mock::FileSystemDataBase>(mock::FileSystemDataBase::new()).await;
}

#[cfg(test)]
mod tests {
    use super::*;

    fn remove_relevance(res: &mut api::SearchResult) {
        for doc in &mut res.search_results {
            doc.relevance = None;
        }
    }

    #[actix_rt::test]
    async fn test_service() {
        let thread = std::thread::spawn(|| {
            mock::wait_for("http://localhost:12345").unwrap();
            let mut res = mock::request::<api::BaseSearchRequest, api::SearchResult>("http://localhost:12345/search", api::BaseSearchRequest{
                text: "".into(),
                limit: Some(2),
                geo: Some(api::GeoResponse{
                    region: Some("Some Region".into())
                }),
                user: None,
            }).unwrap();
            remove_relevance(&mut res);

            assert_eq!(res.search_results, vec![
                api::Document{
                    document: "some another document".into(),
                    key: "some_another_key".into(),
                    key_md5: "3".into(),
                    relevance: None,
                },
                api::Document{
                    document: "some document".into(),
                    key: "some_key".into(),
                    key_md5: "1".into(),
                    relevance: None,
                },
            ]);

            nix::sys::signal::raise(nix::sys::signal::Signal::SIGINT).unwrap();
        });

        let cfg = config::Config {
            bind_address: "localhost:12345".into(),
            database: "db.csv".into(),
        };
        let mut db = mock::DataBaseMock::new();
        db.add("db.csv".into(), r#"class,key,document,age_from,age_to,region,gender,key_md5
,some_key,some document,0,10,Some Region,"male,female",1
,another_key,another document,15,30,Another Region,"female",2
,some_another_key,some another document,20,25,Some Region,"male",3
"#.into());

        service::run_with_config::<BaseSearch, mock::DataBaseMock>(db, cfg).await;
        thread.join().unwrap();
    }
}
