use rank::{WeightedDoc, Relevance, DocHeap};
use prelude::*;
use crate::config;
use crate::inverted_index::{InvertedIndex, DocId};
use std::collections::{HashMap, HashSet};

pub struct Document {
    pub key: String,
    pub key_md5: String,
    pub body: String,

    class: String,
    age_from: u32,
    age_to: u32,
    genders: HashSet<u32>,
    region: u32,
}

pub struct Restrictions {
    pub age: Option<u32>,
    pub region: Option<String>,
    pub gender: Option<String>,
}

struct IndexedRestrictions {
    pub age: Option<u32>,
    pub region: Option<u32>,
    pub gender: Option<u32>,
}

pub struct DataBase {
    inverted_index: InvertedIndex,
    docs: Vec<Document>,
    genders: HashMap<String, u32>,
    regions: HashMap<String, u32>,
}

impl DataBase {
    pub fn new<D: mock::DataBaseProvider>(config: &config::Config, provider: D) -> Result<Self> {
        let mut db = DataBase{
            inverted_index: InvertedIndex::new(),
            docs: Vec::new(),
            genders: HashMap::new(),
            regions: HashMap::new(),
        };

        db.build(config, provider)?;

        Ok(db)
    }

    fn build<D: mock::DataBaseProvider>(&mut self, config: &config::Config, db: D) -> Result<()> {
        let contents = db.load(&config.database)?;
        let mut rdr = csv::Reader::from_reader(contents.as_bytes());
        for result in rdr.deserialize() {
            let record: api::NewsRecord = result?;
            let docid = DocId(self.docs.len() as u32);
            self.inverted_index.add_document(&record.document, docid);

            let mut genders: HashSet<u32> = HashSet::new();
            for gender in record.genders.split(',') {
                let next = self.genders.len() as u32;
                let id = *self.genders.entry(gender.into()).or_insert(next);
                genders.insert(id);
            }
            let next = self.regions.len() as u32;
            let region = *self.regions.entry(record.region.into()).or_insert(next);

            let document = Document{
                key: record.key,
                key_md5: record.key_md5,
                body: record.document,
                class: record.class,
                age_from: record.age_from,
                age_to: record.age_to,
                genders,
                region,
            };

            self.docs.push(document);
        }
        Ok(())
    }

    pub fn search<'a>(&'a self, query: &str, limit: usize, restrictions: Option<&Restrictions>) -> Vec<WeightedDoc<'a, Document>> {
        let mut docs: DocHeap<'a, Document> = DocHeap::new(limit);
        let mut keys: HashSet<&'a str> = HashSet::new();
        let mut quorum = query.split(' ').count() as isize;

        while docs.size() < limit && quorum >= 0 {
            for doc in self.search_iter(query, quorum as usize, restrictions) {
                if !keys.insert(doc.document.key_md5.as_str()) {
                    continue;
                }

                docs.add(doc);
            }

            quorum -= 1;
        }

        docs.finish()
    }

    fn search_iter(&self, query: &str, quorum: usize, restrictions: Option<&Restrictions>) -> Box<dyn std::iter::Iterator<Item=WeightedDoc<Document>> + '_> {
        let restrictions = restrictions.map(|r| self.make_indexed_restrictions(r));
        if quorum == 0 {
            return Box::new(self.docs.iter()
                .map(move |doc| {
                    let mut rating = 0;
                    if let Some(r) = &restrictions {
                        if let Some(age) = r.age {
                            if age >= doc.age_from && age <= doc.age_to {
                                rating += 1;
                            }
                        }
                        if let Some(region) = r.region {
                            if region == doc.region {
                                rating += 1;
                            }
                        }
                        if let Some(gender) = r.gender {
                            if doc.genders.contains(&gender) {
                                rating += 1;
                            }
                        }
                    }
                    WeightedDoc{
                        document: doc,
                        relevance: Relevance{
                            matched_tokens: 0,
                            key_md5: doc.key_md5.as_ref(),
                            rating,
                        }
                    }
                }));
        }

        Box::new(self.inverted_index
            .make_iterator(query, quorum)
            .map(move |(DocId(i), count)| (&self.docs[i as usize], count))
            .map(move |(doc, count)| {
                let mut rating = 0;
                if let Some(r) = &restrictions {
                    if let Some(age) = r.age {
                        if age >= doc.age_from && age <= doc.age_to {
                            rating += 1;
                        }
                    }
                    if let Some(region) = r.region {
                        if region == doc.region {
                            rating += 1;
                        }
                    }
                    if let Some(gender) = r.gender {
                        if doc.genders.contains(&gender) {
                            rating += 1;
                        }
                    }
                }
                WeightedDoc{
                    document: doc,
                    relevance: Relevance{
                        matched_tokens: count,
                        key_md5: doc.key_md5.as_ref(),
                        rating,
                    }
                }
            }))
    }

    fn make_indexed_restrictions(&self, restrictions: &Restrictions) -> IndexedRestrictions {
        IndexedRestrictions{
            age: restrictions.age,
            region: restrictions.region.as_ref().and_then(|x| self.regions.get(x).map(|r| *r)),
            gender: restrictions.gender.as_ref().and_then(|x| self.genders.get(x).map(|r| *r)),
        }
    }
}
