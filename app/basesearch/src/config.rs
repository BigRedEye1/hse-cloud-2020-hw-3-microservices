#[derive(Clone, Debug, serde::Deserialize)]
pub struct Config {
    pub database: String,
    pub bind_address: String,
}
