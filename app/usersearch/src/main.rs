use prelude::*;

use actix_web::{post, web, App, HttpServer, HttpResponse, Responder};
use std::collections::HashMap;

#[derive(Clone, Debug, serde::Deserialize)]
struct Config {
    database: String,
    bind_address: String,
}

#[derive(Clone)]
struct UserSearch {
    conf: Config,
    database: std::sync::Arc<HashMap<u32, api::UserResponse>>,
}

fn load_database<DB: mock::DataBaseProvider>(conf: &Config, provider: DB) -> Result<HashMap<u32, api::UserResponse>> {
    let mut db: HashMap<u32, api::UserResponse> = HashMap::new();

    let file = provider.load(&conf.database)?;
    let mut rdr = csv::Reader::from_reader(file.as_bytes());
    for result in rdr.deserialize() {
        let record: api::UserRecord = result?;

        db.insert(record.user_id, api::UserResponse{
            gender: Some(record.gender),
            age: Some(record.age),
        });
    }

    Ok(db)
}

#[async_trait::async_trait(?Send)]
impl service::Service for UserSearch {
    type Config = Config;

    fn name() -> &'static str {
        "usersearch"
    }

    async fn new<DB: mock::DataBaseProvider>(conf: Self::Config, db: DB) -> Result<Self> {
        let db = load_database(&conf, db)?;
        Ok(UserSearch{
            conf,
            database: std::sync::Arc::new(db),
        })
    }

    async fn run(self) -> Result<()> {
        log::info!("Starting usersearch server at {}", &self.conf.bind_address);

        let clone = self.clone();
        HttpServer::new(move || App::new().data(clone.clone()).service(search))
            .bind(self.conf.bind_address)?
            .run()
            .await?;

        Ok(())
    }
}

impl UserSearch {
    async fn search(&self, req: api::UserRequest) -> Result<&api::UserResponse> {
        let res = match self.database.get(&req.user_id) {
            Some(res) => res,
            None => &api::UserResponse{
                gender: None,
                age: None,
            }
        };
        Ok(res)
    }
}

#[post("/search")]
async fn search(intsearch: web::Data<UserSearch>, web::Json(request): web::Json<api::UserRequest>) -> impl Responder {
    match intsearch.search(request).await {
        Ok(result) => HttpResponse::Ok().json(result),
        Err(error) => {
            log::error!("Search failed: {}", error);
            HttpResponse::InternalServerError().json(api::Error{ error: error.to_string() })
        }
    }
}

#[actix_web::main]
async fn main() {
    service::run::<UserSearch, mock::FileSystemDataBase>(mock::FileSystemDataBase::new()).await;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[actix_rt::test]
    async fn test_service() {
        let thread = std::thread::spawn(|| {
            mock::wait_for("http://localhost:12347").unwrap();
            let res = mock::request::<api::UserRequest, api::UserResponse>("http://localhost:12347/search", api::UserRequest{
                user_id: 13,
            }).unwrap();

            assert_eq!(res, api::UserResponse{ age: Some(48), gender: Some("male".into()) });

            nix::sys::signal::raise(nix::sys::signal::Signal::SIGINT).unwrap();
        });

        let cfg = Config {
            bind_address: "localhost:12347".into(),
            database: "db.csv".into(),
        };
        let mut db = mock::DataBaseMock::new();
        db.add("db.csv".into(), r#"user_id,gender,age
6,female,73
7,female,37
8,male,44
9,female,75
10,male,75
11,male,45
12,female,40
13,male,48
14,transgender,64
15,third gender,36
16,agender,48
17,transgender,69
18,female,33
"#.into());

        service::run_with_config::<UserSearch, mock::DataBaseMock>(db, cfg).await;
        thread.join().unwrap();
    }
}
