fn main() {
    let url = std::env::var("METASEARCH_URL").unwrap();
    let params = "text=United&user_id=35&ip_addr=4.18.64.10";

    mock::wait_for(&url).unwrap();
    loop {
        let res = mock::get::<api::MetaSearchResponse>(&format!("{}?{}", url, params)).unwrap();
        let search_results = match res {
            api::MetaSearchResponse::Error(_) => continue,
            api::MetaSearchResponse::SearchResults(res) => res,
        };

        assert_eq!(search_results, vec![
            api::Document{
                document: "  Soliman S. Biheiri, the only person convicted in a broad probe into whether Islamic charities in Northern Virginia were financing terrorist organizations, pleaded guilty yesterday to illegally possessing and using a U.S. passport to enter the United States last year.".into(),
                key: "Man Pleads Guilty in Passport Case".into(),
                key_md5: "f69e5df07d9f19f865dd2c0656950c76".into(),
                relevance: None,
            },
            api::Document{
                document: "SEOUL - The head of the United Nations #39; nuclear watchdog, Mr Mohamed El-Baradei, arrived in Seoul yesterday to discuss South Korea #39;s nuclear experiments.".into(),
                key: "IAEA head in Seoul to discuss nuclear activities".into(),
                key_md5: "edaa3a51f2d8cfbcaa774b922359d346".into(),
                relevance: None,
            },
            api::Document{
                document: "UNITED NATIONS, New York For years, Kofi Annan was seen at the United Nations as a Mandela-like figure, a statesman who brought a serene sense of personal confidence and institutional steadiness to the task of settling conflict and a person frequently ".into(),
                key: "Annan is teetering on his pedestal".into(),
                key_md5: "b30c5ef504b8b190d5be787284223e58".into(),
                relevance: None,
            },
            api::Document{
                document: " WASHINGTON (Reuters) - The United States hopes to boost the  availability of electricity throughout Iraq to at least 18  hours a day by the end of next year from 11 to 15 hours now,  the top U.S. aid official said on Friday.".into(),
                key: "U.S. Aims to Boost Electricity in Iraq to 20 Hours".into(),
                key_md5: "a249e2ce2386a60758c8030e18074f1f".into(),
                relevance: None,
            },
            api::Document{
                document: "A couple of years ago Sheikh Zayed ibn Sultan Al-Nahyan, the emir of Abu Dhabi and president of the United Arab Emirates (UAE) patronized the translation and publication of an old Islamic text about the ideal prince.".into(),
                key: "Editorial: The Ideal Prince".into(),
                key_md5: "971e682dc26433bb5a2e6fea9dd0728a".into(),
                relevance: None,
            },
            api::Document{
                document: "Canadian Press - KHARTOUM, Sudan (CP) - Sudan isn't afraid of a U.S.-backed United Nations resolution threatening sanctions over the violence in Darfur, President Omar el-Bashir was quoted as saying Sunday.".into(),
                key: "Sudan says it's not afraid of UN resolution threatening sanctions (Canadian Press)".into(),
                key_md5: "8076a967528e5ec5982bd26ec4cadde6".into(),
                relevance: None,
            },
            api::Document{
                document: "USATODAY.com - President Bush says U.S. forces will stay in Iraq as long as necessary but no longer. John Kerry says he wants to bring U.S. troops home but will not \"cut and run\" before the country is stable. Despite differences over how the United States went to war, either man as president would pursue a similar strategy now, their campaign statements show.".into(),
                key: "Bush, Kerry have similar postwar strategies (USATODAY.com)".into(),
                key_md5: "7bc560589f9652de20a8ed3edbbddd94".into(),
                relevance: None,
            },
            api::Document{
              document: " KABUL (Reuters) - The United States has brokered a  cease-fire between a renegade Afghan militia leader and the  embattled governor of the western province of Herat,  Washington's envoy to Kabul said Tuesday.  \"Our expectation is that the agreement that has been made will  be honored,\" said ambassador Zalmay Khalilzad, adding that the  cease-fire was due to take effect at 4 p.m.".into(),
              key: "U.S. Brokers Cease-fire in Western Afghanistan".into(),
              key_md5: "6da7b77e00df9731a8f34c84d29da5e0".into(),
              relevance  : None,
            },
            api::Document{
                document: "Approximately 500 D.C. United fans gathered at RFK Stadium on Tuesday to celebrate the team's victory in Sunday's MLS Cup.".into(),
                key: "Fans Honor United".into(),
                key_md5: "6be72bd14ef8ea256db05ec6ff792372".into(),
                relevance: None,
            },
            api::Document{
                document: "A PRIEST accused of bulldozing more than 2000 Tutsi villagers to death as they sought sanctuary in his church during Rwanda #39;s 1994 genocide has gone on trial before a United Nations court charged with crimes against humanity.".into(),
                key: "Priest on trial over killing of 2000 Rwandans".into(),
                key_md5: "62bfbbe99edebcfecff77b08bcfab415".into(),
                relevance: None,
            }
        ]);

        break;
    }
}
