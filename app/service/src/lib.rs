use prelude::*;

#[async_trait::async_trait(?Send)]
pub trait Service: Sized {
    type Config: serde::de::DeserializeOwned + std::fmt::Debug;

    async fn new<DB: mock::DataBaseProvider>(config: Self::Config, db: DB) -> Result<Self>;

    async fn run(self) -> Result<()>;

    fn name() -> &'static str;
}

pub async fn run<S: Service, D: mock::DataBaseProvider>(db: D) {
    if let Err(e) = do_run::<S, D>(db).await {
        log::error!("Service failed:\n\n{:?}", e);
    } else {
        log::info!("Service successfully stopped");
    }
}

pub async fn run_with_config<S: Service, D: mock::DataBaseProvider>(db: D, config: S::Config) {
    if let Err(e) = do_run_with_config::<S, D>(db, config).await {
        log::error!("Service failed:\n\n{:?}", e);
    } else {
        log::info!("Service successfully stopped");
    }
}

async fn do_run<S: Service, D: mock::DataBaseProvider>(db: D) -> Result<()> {
    dotenv::dotenv().ok();
    env_logger::init();

    let name = S::name();
    log::info!("Building service {}", name);

    let config = parse_config::<S::Config>(name).context("Failed to parse config")?;
    log::info!("Successfully parsed config {:?}", config);

    do_run_with_config::<S, D>(db, config).await
}

async fn do_run_with_config<S: Service, D: mock::DataBaseProvider>(db: D, config: S::Config) -> Result<()> {
    let service = S::new(config, db).await.context("Failed to build service")?;

    log::info!("Running service");
    service.run().await?;

    Ok(())
}

fn parse_config<'a, Config: serde::de::DeserializeOwned + Sized>(prefix: &'a str) -> Result<Config> {
    log::info!("Parsing config");

    let mut s = config::Config::new();
    s.merge(config::Environment::with_prefix(prefix))?;

    s.try_into().map_err(|e| anyhow::Error::new(e))
}
