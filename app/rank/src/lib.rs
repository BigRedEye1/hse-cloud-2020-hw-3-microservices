use std::collections::BinaryHeap;

#[derive(Ord, Eq, PartialEq, PartialOrd)]
pub struct Relevance<'a> {
    pub matched_tokens: u32,
    pub rating: u32,
    pub key_md5: &'a str,
}

pub struct WeightedDoc<'a, T> {
    pub document: &'a T,
    pub relevance: Relevance<'a>,
}

impl<T> Ord for WeightedDoc<'_, T> {
    fn cmp<'a, 'b>(&'a self, other: &'b Self) -> std::cmp::Ordering {
        other.relevance.cmp(&self.relevance)
    }
}

impl<T> PartialEq for WeightedDoc<'_, T> {
    fn eq<'a, 'b>(&'a self, other: &'b Self) -> bool {
        self.relevance.eq(&other.relevance)
    }
}

impl<T> Eq for WeightedDoc<'_, T> {
}

impl<T> PartialOrd for WeightedDoc<'_, T> {
    fn partial_cmp<'a, 'b>(&'a self, other: &'b Self) -> Option<std::cmp::Ordering> {
        other.relevance.partial_cmp(&self.relevance)
    }
}

pub struct DocHeap<'a, T> {
    heap: BinaryHeap<WeightedDoc<'a, T>>,
    top: usize,
}

impl<'a, T> DocHeap<'a, T> {
    pub fn new(top: usize) -> Self {
        DocHeap{
            heap: BinaryHeap::new(),
            top
        }
    }

    pub fn add(&mut self, doc: WeightedDoc<'a, T>) {
        self.heap.push(doc);
        if self.heap.len() > self.top {
            self.heap.pop();
        }
    }

    pub fn finish(self) -> Vec<WeightedDoc<'a, T>> {
        self.heap.into_sorted_vec()
    }

    pub fn size(&self) -> usize {
        self.heap.len()
    }
}
