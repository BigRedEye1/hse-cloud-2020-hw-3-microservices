use prelude::*;
use std::collections::HashMap;

pub trait DataBaseProvider {
    fn load(&self, path: &str) -> Result<String>;
}

pub struct DataBaseMock {
    files: HashMap<String, String>,
}

impl DataBaseMock {
    pub fn new() -> DataBaseMock {
        DataBaseMock{ files: HashMap::new() }
    }

    pub fn add(&mut self, key: String, value: String) {
        self.files.insert(key, value);
    }
}

impl DataBaseProvider for DataBaseMock {
    fn load(&self, path: &str) -> Result<String> {
        match self.files.get(path) {
            Some(res) => Ok(res.clone()),
            None => Err(anyhow::Error::msg("Not found")),
        }
    }
}

pub struct FileSystemDataBase {
}

impl FileSystemDataBase {
    pub fn new() -> FileSystemDataBase {
        FileSystemDataBase{}
    }
}

impl DataBaseProvider for FileSystemDataBase {
    fn load(&self, path: &str) -> Result<String> {
        let res = std::fs::read_to_string(path)?;
        Ok(res)
    }
}

pub fn request<Req: serde::Serialize, Res: serde::de::DeserializeOwned>(url: &str, req: Req) -> Result<Res> {
    let client = reqwest::blocking::ClientBuilder::new()
        .build()
        .unwrap();

    let res = client
        .post(url)
        .json(&req)
        .send()?
        .json()?;

    Ok(res)
}

pub fn get<Res: serde::de::DeserializeOwned>(url: &str) -> Result<Res> {
    let res = reqwest::blocking::get(url)?
        .json()?;

    Ok(res)
}

pub fn wait_for(url: &str) -> Result<()> {
    loop {
        match reqwest::blocking::get(url) {
            Err(e) => {
                if e.is_connect() {
                    continue;
                }

                return Err(e.into());
            },
            Ok(_) => {
                return Ok(());
            }
        }
    }
}
